# terminal-menu-rs

Create simple menus for the terminal. Linux/Mac/Windows

Examples: https://gitlab.com/xamn/terminal-menu-rs/tree/master/examples

```
> Selection       Second Option
  Do Something   [Yes] No
  Numeric         5.25
  Submenu    
  Exit     
```
