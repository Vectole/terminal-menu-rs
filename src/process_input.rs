use crossterm::event::{Event, KeyCode::{self, *}, Event::Key};
use crate::TMIKind::*;
use crate::utils::*;
use crate::TerminalMenu;

pub fn process_input(menu: &TerminalMenu, event: Event, longest_name: usize) {
    let key_code = match event {
        Key(e) => e.code,
        _ => return,
    };
    let mut menu = menu.write().unwrap();
    let s = menu.selected;

    save_pos();
    move_up((menu.items.len() - s - 1) as u16);
    
    match key_code {
        Left | Char('a') | Right | Char('d') | Enter | Char(' ') => {
            move_right(longest_name as u16 + 6);
            clear_rest_of_line();

            match &mut menu.items[s].kind {
                Button | Submenu(_) => {
                    match key_code {
                        Enter | Char(' ') => {
                            menu.active = false;
                        },
                        _ => (),
                    }
                }
                Scroll {values, selected} => {
                    advance_selected(key_code, selected, values.len());
                    print!("{}", values[*selected]);
                }
                List {values, selected} => {
                    advance_selected(key_code, selected, values.len());
                    move_left(1);
                    for i in 0..values.len() {
                        print!("{}{}{}",
                            if i == *selected { '[' } else { ' ' },
                            values[i],
                            if i == *selected { ']' } else { ' ' },
                        );
                    }
                }
                Numeric {value, step, min, max} => {
                    match key_code {
                        Left | Char('a') => {
                            *value -= *step;
                            if *value < *min {
                                *value = *min;
                            }
                        }
                        Right | Char('d') => {
                            *value += *step;
                            if *value > *max {
                                *value = *max;
                            }
                        }
                        _ => (),
                    }
                    
                    print!("{}", value);
                }
            }
        }
        Up | Char('w') | Down | Char('s') => {
            print!(" ");
            match key_code {
                Up | Char('w') => {
                    if menu.selected == 0 {
                        menu.selected = menu.items.len() - 1;
                        move_down(menu.items.len() as u16 - 1);
                    } else {
                        menu.selected -= 1;
                        move_up(1);
                    }
                }
                Down | Char('s') => {
                    if menu.selected == menu.items.len() - 1 {
                        menu.selected = 0;
                        move_up(menu.items.len() as u16 - 1);
                    } else {
                        menu.selected += 1;
                        move_down(1);
                    }
                }
                _ => (),
            }
            print!("\u{8}>");
        }
        _ => (),
    }

    restore_pos();
}

fn advance_selected(key_code: KeyCode, selected: &mut usize, list_length: usize) {
    match key_code {
        Left | Char('a') => {
            if *selected == 0 {
                *selected = list_length - 1;
            } else {
                *selected -= 1;
            }
        }
        Right | Char('d') | Enter | Char(' ') => {
            if *selected == list_length - 1 {
                *selected = 0;
            } else {
                *selected += 1;
            }
        }
        _ => (),
    }
}