use crossterm::{
    execute,
    cursor,
    terminal,
};
use std::io::{stdout, Write}; 

pub fn move_up(a: u16) {
    if a != 0 {
        execute!(stdout(), cursor::MoveUp(a)).unwrap();
    }
}
pub fn move_down(a: u16) {
    if a != 0 {
        execute!(stdout(), cursor::MoveDown(a)).unwrap();
    }
}
pub fn move_left(a: u16) {
    if a != 0 {
        execute!(stdout(), cursor::MoveLeft(a)).unwrap();
    }
}
pub fn move_right(a: u16) {
    if a != 0 {
        execute!(stdout(), cursor::MoveRight(a)).unwrap();
    }
}
pub fn move_to_beginning() {
    for _ in 0..terminal::size().unwrap().0 {
        print!("\u{8}");
    }
}
pub fn clear_rest_of_line() {
    execute!(stdout(), terminal::Clear(terminal::ClearType::UntilNewLine)).unwrap();
}
pub fn save_pos() {
    execute!(stdout(), cursor::SavePosition).unwrap();
}
pub fn restore_pos() {
    execute!(stdout(), cursor::RestorePosition).unwrap();
}