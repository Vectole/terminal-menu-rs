//! Create simple menus for the terminal!
//!
//! [Examples](https://gitlab.com/xamn/terminal-menu-rs/tree/master/examples)

use std::{
    sync::{Arc, RwLock},
    thread,
    io::{stdout, Write},
    time::Duration
};
use crossterm::{
    execute,
    cursor,
    terminal,
    event,
};

mod utils;
mod process_input;
use utils::*;
use process_input::*;
use TMIKind::*;

type TerminalMenu = Arc<RwLock<TerminalMenuStruct>>;

enum TMIKind {
    Button,
    Scroll{
        values: Vec<String>,
        selected: usize,
    },
    List{
        values: Vec<String>,
        selected: usize,
    },
    Numeric{
        value: f64,
        step: f64,
        min: f64,
        max: f64
    },
    Submenu(TerminalMenu),
}

pub struct TerminalMenuItem {
    name: String,
    kind: TMIKind,
}
/// Make a button terminal-menu item.
/// # Example
/// ```
/// let my_button = terminal_menu::button("My Button");
/// ```
pub fn button(name: &str) -> TerminalMenuItem {
    TerminalMenuItem {
        name: name.to_owned(),
        kind: Button,
    }
}
/// Make a terminal-menu item from which you can select a value from a selection.
/// # Example
/// ```
/// let my_selection = terminal_menu::scroll("My Selection", vec![
///     "First Option",
///     "Second Option",
///     "Third Option",
/// ]);
/// ```
pub fn scroll(name: &str, values: Vec<&str>) -> TerminalMenuItem {
    if values.len() == 0 {
        panic!("values cannot be empty");
    }
    TerminalMenuItem {
        name: name.to_owned(),
        kind: Scroll {
            values: values.iter().map(|&s| s.to_owned()).collect(),
            selected: 0,
        },
    }
}
/// Make a terminal-menu item from which you can select a value from a selection.
/// # Example
/// ```
/// let my_selection = terminal_menu::list("My Selection", vec![
///     "First Option",
///     "Second Option",
///     "Third Option",
/// ]);
/// ```
pub fn list(name: &str, values: Vec<&str>) -> TerminalMenuItem {
    if values.len() == 0 {
        panic!("values cannot be empty");
    }
    TerminalMenuItem {
        name: name.to_owned(),
        kind: List {
            values: values.iter().map(|&s| s.to_owned()).collect(),
            selected: 0,
        },
    }
}
/// Make a terminal-menu item from which you can select a number between specified bounds.
/// # Example
/// ```
/// let my_numeric = terminal_menu::numeric("My Numeric",
///     0.0,  //default
///     0.5,  //step
///     -5.0, //minimum
///     10.0  //maximum
/// );
/// ```
pub fn numeric(name: &str, default: f64, step: f64, min: f64, max: f64) -> TerminalMenuItem {
    TerminalMenuItem {
        name: name.to_owned(),
        kind: Numeric {
            value: default,
            step,
            min,
            max,
        },
    }
}
/// Make a terminal-menu submenu item.
/// # Example
/// ```
/// let my_submenu = terminal_menu::submenu("My Submenu", vec![
///     terminal_menu::list("List", vec!["First", "Second", "Third"]),
///     terminal_menu::button("Back")
/// ]);
/// ```
pub fn submenu(name: &str, items: Vec<TerminalMenuItem>) -> TerminalMenuItem {
    TerminalMenuItem {
        name: name.to_owned(),
        kind: Submenu(menu(items)),
    }
}

pub struct TerminalMenuStruct {
    items: Vec<TerminalMenuItem>,
    selected: usize,
    active: bool,
    exited: bool,
}
impl TerminalMenuStruct {
    /// Returns true if the menu is active (open).
    /// # Example
    /// ```
    /// let is_active = menu.read().unwrap().is_active();
    /// ```
    pub fn is_active(&self) -> bool {
        !self.exited
    }
    /// Returns the name of the selected menu item.
    /// # Example
    /// ```
    /// let selected = menu.read().unwrap().selected_item();
    /// ```
    pub fn selected_item(&self) -> &str {
        &self.items[self.selected].name
    }
    /// Returns the value of the specified selection item.
    /// # Example
    /// ```
    /// let s_value = menu.read().unwrap().selection_value("My Selection");
    /// ```
    pub fn selection_value(&self, name: &str) -> &str {
        for item in &self.items {
            match &item.kind {
                List {values, selected} | Scroll {values, selected} => {
                    if item.name == name {
                        return &values[*selected];
                    }
                },
                _ => (),
            }
        }
        panic!("Item not found or is wrong kind");
    }
    /// Returns the value of the specified numeric item.
    /// # Example
    /// ```
    /// let n_value = menu.read().unwrap().numeric_value("My Numeric");
    /// ```
    pub fn numeric_value(&self, name: &str) -> f64 {
        for item in &self.items {
            match item.kind {
                Numeric {value, ..} => {
                    if item.name == name {
                        return value;
                    }
                },
                _ => (),
            }
        }
        panic!("Item not found or is wrong kind");
    }
    /// Returns the specified submenu.
    /// # Example
    /// ```
    /// let submenu = menu.read().unwrap().get_submenu("My Submenu");
    /// ```
    pub fn get_submenu(&self, name: &str) -> TerminalMenu {
        for item in &self.items {
            match &item.kind {
                Submenu(s) => {
                    if item.name == name {
                        return s.clone();
                    }
                },
                _ => (),
            }
        }
        panic!("Item not found or is wrong kind");
    }
}

/// Create a new terminal-menu.
/// # Example
/// ```
/// let menu = terminal_menu::menu(vec![
///     terminal_menu::list("Do Stuff", vec!["Yes", "No"]),
///     terminal_menu::button("Exit")
/// ]);
/// ```
pub fn menu(items: Vec<TerminalMenuItem>) -> TerminalMenu {
    if items.len() == 0 {
        panic!("items cannot be empty");
    }
    Arc::new(RwLock::new(TerminalMenuStruct {
        items,
        selected: 0,
        active: false,
        exited: true
    }))
}
/// Shortcut to getting the selected item as a String.
/// # Example
/// ```
/// let selected = terminal_menu::selected_item(&menu);
/// ```
pub fn selected_item(menu: &TerminalMenu) -> String {
    menu.read().unwrap().selected_item().to_owned()
}
/// Shortcut to getting the value of the specified selection item as a String.
/// # Example
/// ```
/// let s_value = terminal_menu::selection_value(&menu, "Selection");
/// ```
pub fn selection_value(menu: &TerminalMenu, item: &str) -> String {
    menu.read().unwrap().selection_value(item).to_owned()
}
/// Shortcut to getting the value of the specified numeric item.
/// # Example
/// ```
/// let s_value = terminal_menu::numeric_value(&menu, "Selection");
/// ```
pub fn numeric_value(menu: &TerminalMenu, item: &str) -> f64 {
    menu.read().unwrap().numeric_value(item)
}
/// Shortcut to getting the specified submenu.
/// # Example
/// ```
/// let submenu = terminal_menu::get_submenu(&menu, "Submenu");
/// ```
pub fn get_submenu(menu: &TerminalMenu, item: &str) -> TerminalMenu {
    menu.read().unwrap().get_submenu(item)
}

fn print_menu(menu: &TerminalMenuStruct, longest_name: usize, selected: usize) {
    for i in 0..menu.items.len() {
        print!("{} {}    ", if i == selected { '>' } else { ' ' }, menu.items[i].name);
        for _ in menu.items[i].name.len()..longest_name {
            print!(" ");
        }
        match &menu.items[i].kind {
            Button | Submenu(_) => {},
            Scroll {values, selected} => print!("{}", values[*selected]),
            List {values, selected} => {
                move_left(1);
                for j in 0..values.len() {
                    print!("{}{}{}",
                           if j == *selected {'['} else {' '},
                           values[j],
                           if j == *selected {']'} else {' '},
                    );
                }
            }
            Numeric {value, ..} => print!("{}", value),
        }
        if i != menu.items.len() - 1 {
            println!();
        } else {
            move_to_beginning();
            stdout().flush().unwrap();
        }
    }
}

fn run_menu(menu: TerminalMenu) {

    //set active
    {
        let mut menu = menu.write().unwrap();
        menu.active = true;
        menu.exited = false;
    }

    execute!(stdout(), cursor::Hide).unwrap();

    //print initially
    let mut longest_name = 0;
    {
        let menu = menu.read().unwrap();
        for item in &menu.items {
            if item.name.len() > longest_name {
                longest_name = item.name.len();
            }
        }
        print_menu(&menu, longest_name, menu.selected);
    }

    let _ = terminal::enable_raw_mode();

    while menu.read().unwrap().active {            
        if let Ok(event) = event::read() {
            process_input(&menu, event, longest_name);
        }
        thread::sleep(Duration::from_millis(10));
    }

    execute!(stdout(),
        cursor::MoveUp(menu.read().unwrap().items.len() as u16 - 1),
        terminal::Clear(terminal::ClearType::FromCursorDown),
        cursor::Show
    ).unwrap();
    
    let _ = terminal::disable_raw_mode();

    let mut runagain = false;
    {
        let menu_rd = menu.read().unwrap();
        match &menu_rd.items[menu_rd.selected].kind {
            Submenu(submenu) => {
                submenu.write().unwrap().selected = 0;
                run(submenu);
                runagain = true;
            }
            _ => (),
        }
    }

    if runagain {
        run(&menu);
    }
    else {
        menu.write().unwrap().exited = true;
    }
}

/// Activate (open) the menu.
/// Menus will deactivate when button items are pressed or
/// deactivated manually.
/// # Example
/// ```
/// terminal_menu::activate(&menu);
/// ```
pub fn activate(menu: &TerminalMenu) {
    let menu = menu.clone();
    thread::spawn(move || {
        run_menu(menu);
    });
}
/// Deactivate (exit) a menu manually.
/// # Example
/// ```
/// terminal_menu::deactivate(&menu);
/// ```
pub fn deactivate(menu: &TerminalMenu) {
    menu.write().unwrap().active = false;
    wait_for_exit(menu);
}
/// Wait for menu to deactivate (exit).
/// # Example
/// ```
/// terminal_menu::wait_for_exit(&menu);
/// ```
pub fn wait_for_exit(menu: &TerminalMenu) {
    loop {
        thread::sleep(Duration::from_millis(10));
        if menu.read().unwrap().exited {
            break;
        }
    }
}
/// Activate the menu and wait for it to deactivate (exit).
/// # Example
/// ```
/// terminal_menu::run(&menu);
/// ```
pub fn run(menu: &TerminalMenu) {
    run_menu(menu.clone());
}